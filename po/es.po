# Spanish translation for typography.
# Copyright (C) 2024 typography's COPYRIGHT HOLDER
# This file is distributed under the same license as the typography package.
# Julián Villodre <jvillodrede@gmail.com>, 2024.
# Daniel Mustieles <daniel.mustieles@gmail.com>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: typography master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/typography/-/"
"issues\n"
"POT-Creation-Date: 2024-01-30 07:58+0000\n"
"PO-Revision-Date: 2024-02-28 12:12+0100\n"
"Last-Translator: Daniel Mustieles <daniel.mustieles@gmail.com>\n"
"Language-Team: Spanish - Spain <gnome-es-list@gnome.org>\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 45.3\n"

#: data/org.gnome.design.Typography.desktop.in:3
#: data/org.gnome.design.Typography.metainfo.xml.in:8 src/window.c:82
#: src/window.ui:6
msgid "Typography"
msgstr "Typography"

#: data/org.gnome.design.Typography.desktop.in:4
#: data/org.gnome.design.Typography.metainfo.xml.in:9
msgid "Look up text styles"
msgstr "Buscar estilos de texto"

#: data/org.gnome.design.Typography.metainfo.xml.in:11
msgid "Tool for working with the GNOME typography design guidelines."
msgstr ""
"Herramienta para trabajar con las pautas de diseño tipográfico de GNOME."

#. Translators: This is my name
#: data/org.gnome.design.Typography.metainfo.xml.in:31
msgid "Zander Brown"
msgstr "Zander Brown"

#: src/window.c:77
#, c-format
msgid "Copyright © %s Zander Brown"
msgstr "Copyright © %s Zander Brown"

#. Translators: Credit yourself here
#: src/window.c:88
msgid "translator-credits"
msgstr "Julián Villodre <jvillodrede@gmail.com>, 2024"

#: src/window.ui:15
msgid "Main Menu"
msgstr "Menú principal"

#: src/window.ui:53
msgid "Title 1"
msgstr "Título 1"

#: src/window.ui:75
msgid "Title 2"
msgstr "Título 2"

#: src/window.ui:97
msgid "Title 3"
msgstr "Título 3"

#: src/window.ui:119
msgid "Title 4"
msgstr "Título 4"

#: src/window.ui:141
msgid "Heading"
msgstr "Encabezado"

#: src/window.ui:163
msgid "Body"
msgstr "Cuerpo del texto"

#: src/window.ui:185
msgid "Caption Heading"
msgstr "Encabezado de la descripción"

#: src/window.ui:207
msgid "Caption"
msgstr "Descripción"

#: src/window.ui:236 src/window.ui:247
msgid "Quotation"
msgstr "Cita"

#: src/window.ui:258
msgid "Time / Ratio"
msgstr "Hora / tasa"

#: src/window.ui:270
msgid "Multiplication"
msgstr "Multiplicación"

#: src/window.ui:282
msgid "Ellipsis"
msgstr "Elipsis"

#: src/window.ui:294
msgid "Apostrophe"
msgstr "Apóstrofe"

#: src/window.ui:306
msgid "Bullet"
msgstr "Viñeta"

#: src/window.ui:318
msgid "Range"
msgstr "Rango"

#: src/window.ui:357
msgid "Design _Guidelines<sup>↗</sup>"
msgstr "Pautas_de diseño<sup>↗</sup>"

#: src/window.ui:364
msgid "_About Typography"
msgstr "_Acerca de Typography"
